<?php

namespace App\Controller;

use App\Repository\TicketRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     *@Route("/", name="home")
     *@IsGranted("ROLE_USER")
     */
    public function index(TicketRepository $ticketRepository): Response
    {
        $tickets = $this->getUser()->getTickets();

        return $this->render('home/index.html.twig', [
            'tickets' => $tickets,
            'controller_name' => 'HomeController',
        ]);
    }
}