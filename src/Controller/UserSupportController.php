<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Repository\CategoryRepository;
use App\Repository\TicketRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPPORT")
 */
class UserSupportController extends AbstractController
{
    #[Route('/user/support', name: 'user_support')]
    public function index(TicketRepository $ticketRepository, CategoryRepository $categoryRepository): Response
    {
        return $this->render('user_support/index.html.twig', [
            'category' => $categoryRepository->findAll(),
            'tickets' => $ticketRepository->findAll(),
        ]);
    }

    #[Route('/user/support/{id}', name: 'user_support_edit')]
    public function edit(Ticket $ticket): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ticket->setStatus('fermer');
        $entityManager->persist($ticket);
        $entityManager->flush();

        return $this->redirectToRoute('user_support');
    }

    #[Route('/user/support/{id}', name: 'user_support_close')]
    public function close(Ticket $ticket): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ticket->setStatus('fermer');
        $entityManager->persist($ticket);
        $entityManager->flush();

        return $this->redirectToRoute('user_support');
    }
}