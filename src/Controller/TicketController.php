<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Ticket;
use App\Form\ResponseFormType;
use App\Form\TicketType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TicketController extends AbstractController
{
    /**
     * @Route("/ticket/new", name="app_ticket_create")
     * @Route("/ticket/{id}/edit", name="app_ticket_edit")
     */
    public function index(Ticket $ticket = null, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$ticket) {
            $ticket = new Ticket();
        }

        $form = $this->createForm(TicketType::class, $ticket);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$ticket->getId()) {
                $ticket->setDate(new \DateTime());
                $ticket->setUser($this->getUser());
                $ticket->setStatus('ouvert');
            }

            $manager->persist($ticket);
            $manager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('ticket/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ticket/{id}", name="app_ticket_show")
     */
    public function show(Ticket $ticket, Request $request, EntityManagerInterface $manager): Response
    {
        $response = new \App\Entity\Response();

        $form = $this->createForm(ResponseFormType::class, $response);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$response->getId()) {
                $response->setDate(new \DateTime());
                $response->setUser($this->getUser());
                $response->setTicket($ticket);
            }

            $manager->persist($response);
            $manager->flush();

            return $this->redirectToRoute('app_ticket_show', ['id' => $ticket->getId()]);
        }

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ticket/edit/{id}", name="app_ticket_edit")
     */
    public function edit(Ticket $ticket, Request $request, EntityManagerInterface $manager): Response
    {
        $form = $this->createFormBuilder($ticket)
            ->add('category', EntityType::class, ['class' => Category::class, 'choice_label' => 'name'])
            ->getForm()
        ;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$ticket->getId()) {
                $ticket->setCategory($form->getData('category'));
            }

            $manager->persist($ticket);
            $manager->flush();

            return $this->redirectToRoute('user_support', ['id' => $ticket->getId()]);
        }

        return $this->render('user_support/edit.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }
}